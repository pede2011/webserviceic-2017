/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clientservlet;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Paths;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.WebServiceRef;
import org.tempuri.WebService1;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.Part;
import javax.xml.bind.DatatypeConverter;

//import com.cdyne.ws.Check;
//import com.cdyne.ws.Words;
//import java.util.List;
/**
 *
 * @author Gef
 */

@WebServlet(name = "SpellCheckServlet", urlPatterns = {"/SpellCheckServlet"})
@MultipartConfig
public class SpellCheckServlet extends HttpServlet {

    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/localhost_50148/WebService1.asmx.wsdl")
    private WebService1 service;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String AddPicture= request.getParameter("AddPicture");
            
            if(AddPicture!= null){
            int studentDNI= Integer.parseInt(request.getParameter("DNI"));
            Part part= request.getPart("Image");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>AddProfilePic</title>");
            out.println("</head>");
            out.println("<body style=\"background-color:lightgrey;\">");   
            
            if(part!= null){
                InputStream myStream= part.getInputStream();
                byte[] mydata=getBytes(myStream);
                String str = DatatypeConverter.printBase64Binary(mydata);//Base64 String                
                out.println("<br> Image: <img src=\"data:image/png;base64,"+ str+"\" width=\"150\" height=\"150\" >");//Displaying Image
                out.println("<p>Added to: "+studentDNI+"</p>");
                addProfilePicture(studentDNI,str);
                //out.println("<p> <b>"+str+"</b></p>");                
            }            
            out.println("</form>");
            out.println("</body>");
            }
            MainMenuButtons(request,out);      
        }
    }
    //extracted from http://www.java2s.com/Tutorial/Java/0180__File/GetbytesfromInputStream.htm
    public static byte[] getBytes(InputStream is) throws IOException {

    int len;
    int size = 1024;
    byte[] buf;

    if (is instanceof ByteArrayInputStream) {
      size = is.available();
      buf = new byte[size];
      len = is.read(buf, 0, size);
    } else {
      ByteArrayOutputStream bos = new ByteArrayOutputStream();
      buf = new byte[size];
      while ((len = is.read(buf, 0, size)) != -1)
        bos.write(buf, 0, len);
      buf = bos.toByteArray();
    }
    return buf;
  }
    
    private void MainMenuButtons(HttpServletRequest request,PrintWriter out){
        String AddStudent= request.getParameter("AddStudent");
        String AddGrade= request.getParameter("AddGrade");
        
        String GetStudentGrades= request.getParameter("GetStudentFinalGrades");
        String GetAssignatureGrades= request.getParameter("GetAssignatureGrades");
        String GetGrades= request.getParameter("GetGrades");
        if(GetGrades!=null){
             out.println(getGrades());
        }
        
        if(GetStudentGrades!= null){
            int studentDNI= Integer.parseInt(request.getParameter("DNI"));
            out.println("<html>");
            out.println("<head>");
            out.println("<title>GetStudentGrades</title>");
            out.println("</head>");
            out.println("<body style=\"background-color:lightgrey;\">");   
            String studentGrades= getStudentFinalScores(studentDNI);
            
            out.println("<p> <b>"+studentGrades+"</b></p>");
            out.println("</form>");
            out.println("</body>");
        }
        if(GetAssignatureGrades!=null){
            String studentGrades= request.getParameter("AssignatureName");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>GetAssignatureGrades</title>");
            out.println("</head>");
            out.println("<body style=\"background-color:lightgrey;\">");   
            studentGrades= getAssignatureFinalScores(studentGrades);
            
            out.println("<p> <b>"+studentGrades+"</b></p>");
            out.println("</form>");
            out.println("</body>");
        }
        
        if(AddStudent!= null){
            String studentName= request.getParameter("AddStudentName");
            int studentDNI= Integer.parseInt(request.getParameter("DNI"));
            //Create html file
            out.println("<html>");
            out.println("<head>");
            out.println("<title>AddStudent</title>");
            out.println("</head>");
            out.println("<body style=\"background-color:lightgrey;\">");   
            String addStudentResult= addStudent(studentName,studentDNI);
            
            out.println("<p> <b>"+addStudentResult+"</b></p>");
            out.println("</form>");
            out.println("</body>");
        }
        if(AddGrade!= null){
            String assignatureName= request.getParameter("AssignatureName");
            int DNI= Integer.parseInt(request.getParameter("DNI"));
            int grade= Integer.parseInt(request.getParameter("AssignatureGrade"));
            //Create html file
            out.println("<html>");
            out.println("<head>");
            out.println("<title>AddGrade</title>");
            out.println("</head>");
            out.println("<body style=\"background-color:lightgrey;\">");   
            String AddAssignature= addAssignatureScore(DNI,assignatureName,grade);
            
            out.println("<p> <b>"+AddAssignature+"</b></p>");
            out.println("</form>");
            out.println("</body>");
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String addStudent(String name, int dni) {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        org.tempuri.WebService1Soap port = service.getWebService1Soap();
        return port.addStudent(name, dni);
    }
    private String getStudentFinalScores(int dni) {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        org.tempuri.WebService1Soap port = service.getWebService1Soap();
        return port.getStudentFinalScores(dni);
    }

    private String addAssignatureScore(int dni, java.lang.String assignature, int score) {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        org.tempuri.WebService1Soap port = service.getWebService1Soap();
        return port.addAssignatureScore(dni, assignature, score);
    }

    private String getAssignatureFinalScores(java.lang.String assignature) {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        org.tempuri.WebService1Soap port = service.getWebService1Soap();
        return port.getAssignatureFinalScores(assignature);
    }

    private String addProfilePicture(int dni, java.lang.String string) {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        org.tempuri.WebService1Soap port = service.getWebService1Soap();
        return port.addProfilePicture(dni, string);
    }

    private String getGrades() {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        org.tempuri.WebService1Soap port = service.getWebService1Soap();
        return port.getGrades();
    }

}
