﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

namespace WebApplication1 {
    [Serializable]
    public class Student
    {
        public int dni=0;
        string name="";
        string image = "";
        Dictionary<string, List<int>> assignatures;//<assignature name, list of scores for that assignature
        public Dictionary<string, int> finalScores;//<assignature name, int final score>
        public Student(string _name, int _dni){//-0 or null to ignore that setter
            assignatures = new Dictionary<string, List<int>>();//won't be set on constructor
            setName(_name);
            SetDNI(_dni);            
        }
        public void SetDNI(int _dni){
            if(_dni != 0) {
                dni = _dni;
            }            
        }
        public bool GetDNI(int _dni)
        {
            if(dni== _dni)
            {
                return true;
            }
            return false;
        }
        public int GetDNI() {
            return dni;
        }
        public string GetName() {
            return name;
        }
        public void setName(string _name)
        {
            if(_name!=null && _name != "") {
                name = _name;
            }                     
        }
        public int getDNIWithName(string _name){
            if(name==_name) {
                return dni;
            }
            return 0;
        }
        public bool studentExist(int _dni) {
            if(_dni==dni) {
                return true;
            }
            return false;
        }
        public void AddScore(string _assignature, int _score) {
            if(assignatures.ContainsKey(_assignature)){//contains key then just add a score, if doesn't create a new one
                assignatures[_assignature].Add(_score);
            }
            else {
                assignatures.Add(_assignature, new List<int>());
                assignatures[_assignature].Add(_score);
            }
        }
        public Dictionary<string, int> GetAverageScore() {//return list of strings ready to print
            List<string> keys= assignatures.Keys.ToList<string>();
            finalScores=  new Dictionary<string, int>();//resets dictionary
            int average=0;
            for(int i = 0; i < keys.Count; i++) {
                int numberOfScores = assignatures[keys[i]].Count;//scores inside this assignature
                for(int j = 0; j < numberOfScores; j++) {
                    average += assignatures[keys[i]][j];
                }
                average = average / numberOfScores;//get average __ "Aca se promedia para abajo muchachos"
                finalScores.Add(keys[i], average);
                average = 0;
            }
            return finalScores;
        }
        public bool AssignatureExist(string _assignature) {
            if(assignatures.ContainsKey(_assignature)) {
                return true;
            }
            return false;
        }
        public int GetAssignatureAverageScore(string _assignature) {//gets the average result of a single assignature
            int average=0;
            int numberOfScores = assignatures[_assignature].Count;//scores inside this assignature
            for(int i = 0; i < numberOfScores; i++) {
                average += assignatures[_assignature][i];
            }
            average = average / numberOfScores;
            return average;
        }
        public string EncodeWithBase64(string _string) {
            string encodedString="";
            encodedString= Convert.ToBase64String(Encoding.UTF8.GetBytes(_string));
            return encodedString;
        }
        public string DecodeWithBase64(string _string) {
            string decodedString = "";
            //decodedString = DecodeBase64(_string);
            return decodedString;
        }
        public string AddPicture(string _picture) {
            image = _picture;
            return "Picture added";
        }
        public string GetPicture() {
            return image;
        }
    }
}