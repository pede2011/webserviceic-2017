﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.IO;

namespace WebApplication1
{
    /// <summary>
    /// Descripción breve de WebService1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class WebService1 : System.Web.Services.WebService{
        //HttpContext currentContext;
        string fileName;
        string filePath;
        //HttpRequest request;
        //HttpResponse response;
        List<Student> students= null;
        public WebService1() {
            BinaryWriter bw;
            //currentContext= HttpContext.Current;
            //if(students== null && currentContext.Application.Get("list")!= null) {
            //    students= (List<Student>)currentContext.Application.Get("ObjectoDo");
            //}
            fileName = "Database.txt";
            filePath= AppDomain.CurrentDomain.BaseDirectory + fileName;
            if(students == null && !File.Exists(filePath)) {//Gets student list if binary exists
                File.Create(filePath).Close();
                students = new List<Student>();
                WriteToBinaryFile<List<Student>>(filePath, students);
                //students = ReadFromBinaryFile<List<Student>>(filePath);
            }            
        }
        protected void Session_Start(object sender, EventArgs e)
        {
            Session["mySession"] = "Gowtham";
        }        
        [WebMethod]
        public string addStudent(string _name,int _dni){
            if(_name== "" || _dni == 0) {
                return "Enter a correct Name and DNI";
            }
            //if(currentContext.Application.Get("list") == null) {//if Students don't exist on context, create it
            //    currentContext.Application.Set("list", new List<Student>());                
            //}

            //students = (List<Student>)currentContext.Application.Get("list");
            if(File.Exists(filePath)) {//Gets student list if binary exists
                students = ReadFromBinaryFile<List<Student>>(filePath);
            }
            else {
                students = new List<Student>();
            }
            for(int i = 0; i < students.Count; i++) {//search for existing DNI
                if(students[i].GetDNI(_dni)) {
                    return "That DNI already exists";
                }
            }
            Student _newStudent = new Student(_name, _dni);//set values
            students.Add(_newStudent);
            //currentContext.Application.Set("list", students);
            WriteToBinaryFile<List<Student>>(filePath, students);
            return "Student: " + _name + "  DNI: " + _dni;
        }
        [WebMethod]
        public string StudentExist(int _dni) {
            //students = (List<Student>)currentContext.Application.Get("list");
            if(File.Exists(filePath)) {//Gets student list if binary exists
                students = ReadFromBinaryFile<List<Student>>(filePath);
            }
            if(students == null) {
                return "There are no students in the database.";
            }
            for(int i = 0; i < students.Count; i++) {
                if(students[i].studentExist(_dni)) {
                    return "There is a student associated to "+_dni;
                }
            }
            return "There is no student associated to "+_dni;
        }
        [WebMethod]
        public string AddAssignatureScore(int _dni,string _assignature, int _score) {
            //students = (List<Student>)currentContext.Application.Get("list");
            if(File.Exists(filePath)) {//Gets student list if binary exists
                students = ReadFromBinaryFile<List<Student>>(filePath);
            }
            if(students == null) {
                return "There are no students in the database.";
            }
            for(int i = 0; i < students.Count; i++) {
                if(students[i].GetDNI(_dni)) {
                    students[i].AddScore(_assignature, _score);
                    //currentContext.Application.Set("list", students);
                    WriteToBinaryFile<List<Student>>(filePath, students);
                    return "Grade: " + _score + " added to :" + students[i].GetName();
                }
            }
            return "Wrong DNI";
        }
        [WebMethod]
        public string GetStudentFinalScores(int _dni) {
            //students = (List<Student>)currentContext.Application.Get("list");
            if(File.Exists(filePath)) {//Gets student list if binary exists
                students = ReadFromBinaryFile<List<Student>>(filePath);
            }
            string finalString = "";
            if(students == null) {
                return "There are no students in the database.";
            }
            for(int i = 0; i < students.Count; i++) {//for all the students
                if(students[i].GetDNI(_dni)){                    
                    Dictionary<string, int> finalScores = new Dictionary<string, int>();
                    finalScores = students[i].GetAverageScore();
                    List<string> assignature = students[i].finalScores.Keys.ToList<string>();
                    finalString +=students[i].GetName()+"  DNI:"+ students[i].GetDNI()+" \n" + "\n\n";
                    for(int j = 0; j < assignature.Count; j++) {//for number of assignatures
                        finalString +="Assignature :"+assignature[j]+" Final grade:"+students[i].finalScores[assignature[j]]+"\n" + "\n\n";
                    }
                    if (students[i].GetPicture() != "")
                    {//if the student has a picture display it
                        finalString += "<br> : <img src=\"data:image/png;base64," + students[i].GetPicture() + "\" width=\"150\" height=\"150\">";//Displaying Image
                    }
                }
            }
            if(finalString != "") {
                return finalString;
            }
            return "Student doesn't exist or hasn't been properly assigned to the system";
        }
        [WebMethod]
        public string GetAssignatureFinalScores(string _assignature) {
            //students = (List<Student>)currentContext.Application.Get("list");
            if(File.Exists(filePath)) {//Gets student list if binary exists
                students = ReadFromBinaryFile<List<Student>>(filePath);
            }
            string finalString = _assignature+"    \n";
            if(students == null) {
                return "There are no students in the database.";
            }
            for(int i = 0; i < students.Count; i++) {
                if(students[i].AssignatureExist(_assignature)) {
                    finalString += "      "+students[i].GetName() + "  " + students[i].GetAssignatureAverageScore(_assignature)+ "\n\n";
                }
                if (students[i].GetPicture() != "")
                {//if the student has a picture display it
                    finalString += "<br> : <img src=\"data:image/png;base64," + students[i].GetPicture() + "\" width=\"150\" height=\"150\">";//Displaying Image
                }
            }
            if(finalString!= _assignature + "    ") {
                return finalString;
            }
            /*
             * create list of strings
             * check if student has the assignature
             * add to list the value of student[i].GetSignatureFinalScore()
             * return all the student name dni and final score
             */
            return "Assignature doesn't exist or hasn't been properly assigned to the system";
        }
        [WebMethod]
        public string AddProfilePicture(int _dni, string _string) {
            //students = (List<Student>)currentContext.Application.Get("list");            
            if(File.Exists(filePath)) {//Gets student list if binary exists
                students = ReadFromBinaryFile<List<Student>>(filePath);
            }
            if(students == null) {
                return "There are no students in the database.";
            }
            for(int i = 0; i < students.Count; i++) {
                if(students[i].GetDNI(_dni)) {
                    string returnInfo= students[i].AddPicture(_string);
                    WriteToBinaryFile<List<Student>>(filePath, students);
                    return returnInfo;
                }
            }            
            return "Something went wrong, please contact Google";
        }
        [WebMethod]
        public string GetGrades() {//of Everything
            //students = (List<Student>)currentContext.Application.Get("list");
            if(File.Exists(filePath)) {//Gets student list if binary exists
                students = ReadFromBinaryFile<List<Student>>(filePath);
            }
            string finalString = "";
            if(students == null) {
                return "There are no students in the database.";
            }
            for(int i = 0; i < students.Count; i++) {//for all the students
                Dictionary<string, int> finalScores = new Dictionary<string, int>();
                finalScores = students[i].GetAverageScore();
                List<string> assignature = students[i].finalScores.Keys.ToList<string>();
                finalString += "<br> "+students[i].GetName() + "  DNI:" + students[i].GetDNI() + " \n" + "\n\n";
                if(students[i].GetPicture() != "") {//if the student has a picture display it
                    finalString += "<br> : <img src=\"data:image/png;base64," + students[i].GetPicture() + "\" width=\"150\" height=\"150\">";//Displaying Image
                }
                for(int j = 0; j < assignature.Count; j++) {//for number of assignatures                    
                    if(students[i].finalScores[assignature[j]] >= 4) {
                        finalString += "<br> Assignature :" + assignature[j] + " State: Passed - Final Grade: " + students[i].finalScores[assignature[j]] + "\n" + "\n\n";
                    }
                    else {
                        finalString += "<br> Assignature :" + assignature[j] + " State: Failed to pass - Final Grade: " + students[i].finalScores[assignature[j]] + "\n" + "\n\n";
                    }                    
                }
            }
            if(finalString != "") {
                return finalString;
            }
            return "Student doesn't exist or hasn't been properly assigned to the system";
        }

        public static void WriteToBinaryFile<T>(string filePath, T objectToWrite, bool append = false) {
            using(Stream stream = File.Open(filePath, append ? FileMode.Append : FileMode.Create)) {
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                binaryFormatter.Serialize(stream, objectToWrite);
            }
        }
        public static T ReadFromBinaryFile<T>(string filePath) {
            using(Stream stream = File.Open(filePath, FileMode.Open)) {
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                return (T)binaryFormatter.Deserialize(stream);
            }
        }
    }
}
